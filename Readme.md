# ASP.NET Core 2.1 Unit Test Examples

A small example project that I created for my talk "Depedency Injection and Unit Tests in ASP.NET Core 2.1". 

* The following tags show different stages of the demo app:
	* 00 - Basic Application & Testing the controller 
	* 01 - Adding Persistency 
	* 02 - Adding dependencies to external services that can be mocked
	* 03 - Coverage Measure (run `dotnet test /p:CollectCoverage=true <- Better solutions?`)

