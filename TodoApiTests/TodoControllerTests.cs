using System;
using Xunit;
using System.Linq;
using System.Threading.Tasks;

// Very nice FOSS library to create more readable tests
// https://fluentassertions.com/
using FluentAssertions;

// Mocking Framework
using Moq;

// Reminder: Show Assembly Reference
using TodoApi.Controllers;
using TodoApi.Models;
using TodoApi.Services;

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TodoApi;
using Microsoft.EntityFrameworkCore;

namespace TodoApiTests {
    public class TodoControllerTests {
        private ApiContext GetContextWithData()
        {
            var options = new DbContextOptionsBuilder<ApiContext>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            var context = new ApiContext(options);
            var seedData = new List<Todo> {
                new Todo("Test 1"),
                new Todo("Test 2"),
                new Todo("Test 3",true),

            };
            context.AddRange(seedData);
            context.SaveChanges();
            return context;
        }

        // Test the new IP service
        [Fact]
        public async Task GetIp() {
            // Arrange
            var context = GetContextWithData();
            var NetworkserviceMock = new Mock<INetworkAddressService>();
            // Mock the one function we need in this call
            NetworkserviceMock.Setup(nas => nas.GetCurrentIp())
                              .Returns(Task.FromResult("42.42.42.42"));
            var controller = new TodosController(context,NetworkserviceMock.Object);

            // Act
            var res = await controller.GetIp();
            // Assertions / Tests
            res.Result.Should().BeNull();
            res.Value.Should().NotBeNull();
            res.Value.Should().Be("42.42.42.42");
        }

        // Get all todos
        [Fact]
        public async Task Index_ReturnsObjectListResult() {
            // Arrange
            var context = GetContextWithData();
            var NetworkserviceMock = new Mock<INetworkAddressService>();
            var controller = new TodosController(context,NetworkserviceMock.Object);

            // Act
            var res = await controller.Get(); // >= 2.0: IActionResult, 2.1 (opt): ActionResult<T>

            // Assertions / Tests
            res.Result.Should().BeNull();
            res.Value.Should().NotBeNull();
            List<Todo> todos = (List<Todo>) res.Value;
            todos.Count.Should().Be(3);
        }

        // Get all open todos
        [Fact]
        public async Task Index_OnlyOpen_ReturnsObjectListResult() {
            // Arrange
            var context = GetContextWithData();
            var NetworkserviceMock = new Mock<INetworkAddressService>();
            var controller = new TodosController(context,NetworkserviceMock.Object);

            // Act
            var res = await controller.Get(onlyOpen:true);

            // Assertions / Tests
            res.Result.Should().BeNull();
            res.Value.Should().NotBeNull();
            List<Todo> todos = (List<Todo>)res.Value;
            todos.Count.Should().Be(2);
            todos.Count(t => t.Done).Should().Be(0);
        }

        // Get a signle todo
        [Fact]
        public async Task GetById_ValidIdReturnTodo() {
            // Arrange
            var context = GetContextWithData();
            var NetworkserviceMock = new Mock<INetworkAddressService>();
            var controller = new TodosController(context,NetworkserviceMock.Object);

            var validId = context.Todos.First().Id;
            // Act
            var res = await controller.Get(validId);

            // Assertions / Tests
            res.Result.Should().BeNull();
            res.Value.Should().NotBeNull();
            Todo todos = res.Value;
            todos.Title.Should().NotBeNull();
        }

        // Get a non existing todo
        [Fact]
        public async Task GetById_InvalidIdReturnsNotFound() {
            // Arrange
            var context = GetContextWithData();
            var NetworkserviceMock = new Mock<INetworkAddressService>();
            var controller = new TodosController(context,NetworkserviceMock.Object);

            // Act
            var res = await controller.Get(12343);

            // Assertions / Tests
            res.Result.Should().NotBeNull();
            res.Value.Should().BeNull();
            res.Result.Should().BeOfType(typeof(NotFoundResult));
        }
    }
}
