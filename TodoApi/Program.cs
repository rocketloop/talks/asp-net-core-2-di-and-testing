﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TodoApi.Models;

namespace TodoApi {
    public class Program {
        /*public static void Main(string[] args) {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
                */

        public ApiContext CreateDbContext(string[] args) {
            var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
            optionsBuilder.UseInMemoryDatabase(databaseName: "TodoDb");

            return new ApiContext(optionsBuilder.Options);
        }

        public static void Main(string[] args) {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope()) {
                var services = scope.ServiceProvider;

                try {
                    AddTestData(services);

                } catch (Exception ex) {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex,"An error occurred while migrating the database.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();

       
        // seed some test Data
        private static void AddTestData(IServiceProvider services) {
            ApiContext context = services.GetRequiredService<ApiContext>();
            var seedData = new List<Todo> {
                new Todo("Prepare ASP.NET Core Talk",true),
                new Todo("Sacrifice to the demo gods",true),
                new Todo("Hold the talk"),
                new Todo("Enjoy Boris' talk"),
                new Todo("Enjoy the rest of the meetup"),
            };

            // Make sure that our test todos exist, but don't replace existing objects
            foreach (Todo todo in seedData) {
                if (!context.Todos.Any(t => t.Title == todo.Title))
                    context.Todos.Add(todo);
            }
            context.SaveChanges();
        }
    }
}

