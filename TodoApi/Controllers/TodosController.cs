﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApi.Models;
using TodoApi.Services;
namespace TodoApi.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class TodosController :ControllerBase {

        private readonly ApiContext apiContext;
        private readonly INetworkAddressService networkAddressService;
        public TodosController(ApiContext apiContext, INetworkAddressService networkAddressService)
        {
            this.apiContext = apiContext;
            this.networkAddressService = networkAddressService;
        }

        // Get the current Ip
        [HttpGet("/ip")]
        public async Task<ActionResult<string>> GetIp() {
            return await networkAddressService.GetCurrentIp();
        }

        // GET api/todos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Todo>>> Get([FromQuery]bool onlyOpen = false) {
            var ret = apiContext.Todos.AsQueryable();
            if (onlyOpen)
                ret= ret.Where(t => !t.Done);
            return await ret.ToListAsync();
        }

        // GET api/todos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Todo>> Get(int id) {
            var ret = await apiContext.Todos.Where(t => t.Id == id).FirstOrDefaultAsync();
            if(ret==null) {
                return NotFound();
            }
            return ret;
        }

        // POST api/todos
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Todo value) {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            apiContext.Todos.Add(value);
            await apiContext.SaveChangesAsync();
            return Ok();
        }


        // PUT api/todos/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id,[FromBody] Todo value) {
            if(!apiContext.Todos.Any(t=>t.Id==id))
                return NotFound();
            value.Id = id;
            apiContext.Todos.Update(value);
            await apiContext.SaveChangesAsync();
            return Ok();

        }

        // DELETE api/todos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id) {
            if (!apiContext.Todos.Any(t => t.Id == id))
                return NotFound();
            apiContext.Todos.Remove(apiContext.Todos.First(t => t.Id == id));
            await apiContext.SaveChangesAsync();
            return Ok();
        }
    }
}
