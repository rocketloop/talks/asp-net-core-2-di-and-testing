﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace TodoApi.Services {
    public class NetworkAddressService :INetworkAddressService {

        public async Task<string> GetCurrentIp()
        {
            var httpClient = new HttpClient();
            var ip = await httpClient.GetStringAsync("https://api.ipify.org");
            return ip;
        }
    }
}
