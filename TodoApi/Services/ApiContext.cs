﻿using Microsoft.EntityFrameworkCore;
using TodoApi.Models;

namespace TodoApi {
    public class ApiContext :DbContext {
        public ApiContext(DbContextOptions<ApiContext> options)
            : base(options) {
        }

        public DbSet<Todo> Todos { get; set; }

    }
}