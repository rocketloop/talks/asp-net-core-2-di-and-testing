﻿using System.Threading.Tasks;

namespace TodoApi.Services {
    public interface INetworkAddressService {
        Task<string> GetCurrentIp();
    }
}