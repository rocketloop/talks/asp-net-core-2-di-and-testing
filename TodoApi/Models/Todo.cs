﻿using System;
using Newtonsoft.Json;

namespace TodoApi.Models {
    public class Todo {
        public int Id { get; set; }

        [JsonRequired]
        public string Title { get; set; }
        public bool Done { get; set; } = false;

        public Todo(string title, bool done=false) {
            Title = title;
            Done = done;
        }
    }
}
